# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.split("").select{|l| l =~ /[^a-z]/}.join("")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  midpoint = (str.length / 2)
  str.length.even? ? str[midpoint-1..midpoint] : str[midpoint]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.select{|x| VOWELS.include?(x)}.count

end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return 1 if num == 0

  result = 1

  num.downto(1) do |x|
    result *= x
  end
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return "" if arr.empty?
  result = arr[0]
  for i in 1..arr.length-1
    result += separator
    result += arr[i]
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.each_with_index do |letter, index|
    index.even? ? str[index] = str[index].downcase :
                 str[index] = str[index].upcase
  end
  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)

  words = str.split(" ")

  words.each_with_index do |word, index|
    if word.length > 4
      words[index] = words[index].reverse
    end
  end
  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a

  arr.each_with_index do |value, index|
    if value % 3 == 0 && value % 5 == 0
      arr[index] = "fizzbuzz"
    elsif value % 3 == 0
      arr[index] = "fizz"
    elsif value % 5 == 0
      arr[index] = "buzz"
    end
  end

  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  (arr.length-1).downto(0) do |x|
    result.push(arr[x])
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  return true if num == 2 || num == 3
  for i in 2..num-1
    if num % i == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  for i in 1..num
    factors << i if num % i == 0
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  fs = factors(num)
  fs.select{|x| prime?(x)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = 0
  evens = 0
  oddball_index = -1

  for i in 0..2
    arr[i].even? ? evens += 1 : odds += 1
  end
  if evens > odds
    return get_odd_int(arr)
  elsif odds > evens
    return get_even_int(arr)
  end

end

def get_even_int(arr)
  result = 0
  arr.each do |x|
    if x.even?
      result = x
      break
    end
  end
  result
end

def get_odd_int(arr)
  result = 0
  arr.each do |x|
    if x.odd?
      result = x
      break
    end
  end
  result
end
